const QR = require("../../utils/qrcode.js")

Page({
  data: {
    hasUserInfo: false,
    noUserInfo: false,
    adPicUrl: ''
  },
  async onReady() {
    this._getUserInfo()
      .then(
        that.setData({
          hasUserInfo: true,
          noUserInfo: false
        })
      )
      .catch(
        that.setData({
          hasUserInfo: false,
          noUserInfo: true
        })
      )

    
  },
  
  

  createQrCode: function (content, canvasId, cavW, cavH) {
    //调用插件中的draw方法，绘制二维码图片
    QR.api.draw(content, canvasId, cavW, cavH)
  },
  
  //获取临时图片的url
  canvasToTempUrl: function (option, that) {
    return new Promise((resolve, reject) => {
      console.log(that)
      wx.canvasToTempFilePath({
        ...option,
        success: function (res) {
          const tempFilePath = res.tempFilePath;
          resolve(tempFilePath)
        },
        fail: function (err) {
          reject(err)
        }
      }, that)
    })
  },

  _getUserAvatarUrl () {
    return new Promise(resolve => {
      wx.getUserInfo({
        success: (res) => {
          resolve(res.userInfo.avatarUrl)
        }
      })
    })
  },

  getuserinfo (res) {
    console.log(res)
    if (res.detail.errMsg === 'getUserInfo:fail auth deny') {
      // 用户允许了拒绝了授权
      wx.showToast({
        title: '授权失败',
        icon: 'none',
        duration: 1500,
        mask: false
      })
    } else if (res.detail.errMsg === 'getUserInfo:ok') {
      // 用户允许了授权, 可以显示绘图界面
      this.setData({
        hasUserInfo: true,
        noUserInfo: false
      })
    }
  },

  _getUserInfo () {
    var that = this
    return new Promise((resolve, reject) => {
      wx.getUserInfo({
        success (res) {
          resolve(res)
        },
        fail (err) {
          reject(err)
        }
      })
    })
  }

})