const QR = require("../../utils/qrcode.js")

Page({
  data: {
    hasUserInfo: false,
    noUserInfo: false,
    adPicOptions: {
      avatarBackground: 'http://127.0.0.1:2828/static/images/test.jpg',
      goodTitleText: 'Funcode so much fun!',
      shareText: 'This is another title!',
      qrCodeInfo: {
        firstname: 'Felix',
        secondname: 'chan',
        age: 18,
        hobby: 'codeing, sport...etc'
      }
    },
    adPicUrl: '',
  },
  async onReady() {
    const that = this
    this._getUserInfo()
      .then(() => {
        // 有授权信息, 可以开始绘制
        that.setData({
          hasUserInfo: true,
          noUserInfo: false
        })
        this.drawAdvertising(this.data.adPicOptions)
      })
      .catch(() => {
        that.setData({
          hasUserInfo: false,
          noUserInfo: true
        })
      })
  },
  
  async drawAdvertising() {
    wx.showLoading({
      title: '正在生成...',
      mask: true
    })
    const qrCodeInfo = JSON.stringify(this.data.adPicOptions.qrCodeInfo)
    // 生成二维码
    await this.createQrCode(qrCodeInfo, 'qrCode', 350, 350)
    // 获取二维码的临时路径
    const qrcodeUrl = await this.canvasToTempUrl({ canvasId: 'qrCode' })
    // 获取用户头像
    const avatarUrl = await this._getUserAvatarUrl()

    const query = wx.createSelectorQuery()
    query.select('#myCanvas')
      .fields({
        node: true,
        size: true
      })
      .exec(async res => {
        const canvas = res[0].node
        const ctx = canvas.getContext('2d')
        // 设置画布大小
        canvas.width = 646;
        canvas.height = 989;

        // 绘制画布背景
        _drawBackgroundColor()
        // 绘制头像背景
        await _drawBackground(this.data.adPicOptions.avatarBackground)

        await _drawAvatar()
        _drawGoodTitleText(this.data.adPicOptions.goodTitleText)
        _drawShareText(this.data.adPicOptions.shareText)
        // 绘制二维码
        await _drawQrcode(qrcodeUrl)
        // 海报绘制完成, 将它转换成图片链接, 并把临时链接给到图片上
        const adPicUrl = await this.canvasToTempUrl({ canvas })
        this.setData({
          adPicUrl
        })

        function  _drawBackgroundColor() {
          ctx.save()
          ctx.beginPath()
          ctx.fillStyle = '#fff';
          ctx.fillRect(0, 0, 646, 989); 
          ctx.restore()
        }

        function _drawBackground (bgPicUrl) {
          return new Promise(async resolve => {
            const headerBgPicture = await _downLoadFileAndLoad(bgPicUrl)
            const picWidth = headerBgPicture.width
            const picHeight = headerBgPicture.height
            const containerWidth = canvas.width
            const containeHeight = 320
            const _calcSizeAndPosition = (picWidth, picHeight, containerWidth, containeHeight) => {
              const containerRatio = containerWidth / containeHeight // 计算容器的宽高比, 这里容器的宽为646, 高为320
              const picRatio = picWidth / picHeight
              if (picRatio === containerRatio) {
                // 如果图片的宽高比和容器的宽高比相等的话, 那我们让图片从(0, 0)w位置开始, 尺寸就是containerWidth和containeHeight
                return {
                  pos: {
                    x: 0,
                    y: 0
                  },
                  width: containerWidth,
                  height: containeHeight
                }
              } else if (picRatio < containerRatio) {
                // 如果图片的宽高比小于容器的宽高比的话, 我们要让图片的宽度和的容器的宽度相等, 这时候, 图片的高度就会超出容器, 我们需要调整起始点的坐标
                return {
                  pos: {
                    x: 0,
                    y: -(picHeight * (containerWidth / picWidth) - containeHeight) / 2
                  },
                  width: containerWidth,
                  height: picHeight * (containerWidth / picWidth)
                }
              } else if (picRatio > containerRatio) {
                // 如果图片的宽高比大于容器的宽高比的话, 我们要让图片的高度和的容器的高度相等, 这时候, 图片的宽度就会超出容器, 我们需要调整起始点的坐标
                return {
                  pos: {
                    x: -(picWidth * (containeHeight / picHeight) - containerWidth) / 2,
                    y: 0
                  },
                  width: picWidth * (containeHeight / picHeight),
                  height: containeHeight
                }
              }
            }
            const sizeAndPos = _calcSizeAndPosition(picWidth, picHeight, containerWidth, containeHeight)
            ctx.save()
            ctx.beginPath()
            ctx.rect(0, 0, 646, 989);
            ctx.clip()
            ctx.drawImage(headerBgPicture, sizeAndPos.pos.x, sizeAndPos.pos.y, sizeAndPos.width, sizeAndPos.height)
            ctx.restore()
            resolve()
            console.log('背景图画完啦')
          })
        }

        async function _drawAvatar () {
          return new Promise(async resolve => {
            const avatar = await _downLoadFileAndLoad(avatarUrl)
            ctx.save()
            ctx.beginPath()
            ctx.arc(323, 320, 81, 0, 2 * Math.PI)
            ctx.strokeStyle = "#fff"
            ctx.lineWidth = 24
            ctx.stroke()
            ctx.clip()
            // 这里由于微信头像拿到的都是正方形的, 所以就没必要重新调整位置啦
            ctx.drawImage(avatar, 242, 239, 162, 162)
            ctx.restore()
            resolve()
          })
        }

        function _drawGoodTitleText (text) {
          console.log('开始绘制商品标题啦')
          ctx.save()
          ctx.beginPath()
          ctx.font = 'normal 33px Arial';
          ctx.textAlign = 'center';
          ctx.textBaseline = 'bottom';
          ctx.fillStyle = '#666';
          ctx.fillText(text, 323, 483); 
          ctx.restore()
        }

        function _downLoadFileAndLoad(bgPicUrl) {
          return new Promise(resolve => {
            wx.downloadFile({
              url: bgPicUrl,
              success(res) {
                let image = canvas.createImage()
                image.src = res.tempFilePath;
                image.onload = function () {
                  resolve(image)
                }
              }
            })
          })
        }

        function _drawShareText (text) {
          console.log('开始绘制分享标题')
          ctx.save()
          ctx.beginPath()
          ctx.font = 'bold 49px Arial';
          ctx.textAlign = 'center';
          ctx.textBaseline = 'bottom';
          ctx.fillStyle = '#000';
          ctx.fillText(text, 323, 582); 
          ctx.restore()
        }

        async function _drawQrcode (qrCodeUrl) {
          return new Promise(async resolve => {
            console.log('开始绘制二维码')
            let qrCode = await _downLoadFileAndLoad(qrCodeUrl)
            ctx.save()
            ctx.beginPath()
            ctx.rect(179, 627, 287, 287)
            ctx.clip()
            ctx.drawImage(qrCode, 174, 622, 298, 297)
            ctx.restore()
            resolve()
          })
        }
      }) 
  },

  createQrCode: function (content, canvasId, cavW, cavH) {
    //调用插件中的draw方法，绘制二维码图片
    QR.api.draw(content, canvasId, cavW, cavH)
  },
  
  //获取临时图片的url
  canvasToTempUrl: function (option) {
    return new Promise((resolve, reject) => {
      // console.log(that)
      wx.canvasToTempFilePath({
        ...option,
        success: function (res) {
          const tempFilePath = res.tempFilePath;
          resolve(tempFilePath)
        },
        fail: function (err) {
          reject(err)
        }
      })
    })
  },

  // 获取用户头像URL
  _getUserAvatarUrl () {
    return new Promise(resolve => {
      wx.getUserInfo({
        success: (res) => {
          resolve(res.userInfo.avatarUrl)
        }
      })
    })
  },

  getuserinfo (res) {
    if (res.detail.errMsg === 'getUserInfo:fail auth deny') {
      // 用户允许了拒绝了授权
      wx.showToast({
        title: '授权失败',
        icon: 'none',
        duration: 1500,
        mask: false
      })
    } else if (res.detail.errMsg === 'getUserInfo:ok') {
      // 用户允许了授权, 可以显示绘图界面
      this.setData({
        hasUserInfo: true,
        noUserInfo: false
      })
      this.drawAdvertising(this.data.adPicOptions)
    }
  },

  _getUserInfo () {
    return new Promise((resolve, reject) => {
      wx.getUserInfo({
        success (res) {
          resolve(res)
        },
        fail (err) {
          reject(err)
        }
      })
    })
  }

})